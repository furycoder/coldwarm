csInterface = new CSInterface();

function ColdWarm() {

    this.settings = new Settings();

    this.foregroundColor = 0;
    this.colors = [];
    this.saturationColors = [];

    this.extensionId = csInterface.getExtensionID();

    this.init = function() {
        var that = this;

        this.settings.load();

        this.enableHotkeyBinding();

        $(window).resize(function() {
            that.resize();
        });

        $(document).on('mouseup', '.cell', function() {
            var color = $(this).css("background-color");
            var rgb = ColorCalc.stringToRGB(color);

            csInterface.evalScript("setColor(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")", function() {
                that.returnFocus();
            });
        });

        for (var i = 0; i < this.settings.evts.length; i++) {
            csInterface.evalScript('makeCodeANumber("' + this.settings.evts[i] + '")', function(res) {
                var event = new CSEvent('com.adobe.PhotoshopRegisterEvent', 'APPLICATION');
                event.extensionId = that.extensionId;
                event.data = res.toString();
                csInterface.dispatchEvent(event);
            });
        }

        csInterface.addEventListener("com.adobe.PhotoshopJSONCallback" + this.extensionId, function() {
            that.loadForegroundColor(function(color) {
                if (that.foregroundColor === color) return;

                that.foregroundColor = color;
                that.refresh();
                that.draw();
            });
        });

        this.changePanelBackgroundColor(csInterface.hostEnvironment.appSkinInfo.panelBackgroundColor.color);
        csInterface.addEventListener(CSInterface.THEME_COLOR_CHANGED_EVENT, function() {
            var skinInfo = JSON.parse(window.__adobe_cep__.getHostEnvironment()).appSkinInfo;
            that.changePanelBackgroundColor(skinInfo.panelBackgroundColor.color);
        });

        this.loadForegroundColor(function(color) {
            that.foregroundColor = color;
            that.initColors();
            that.initSaturationColors();
            that.refresh();
            that.draw();
        });
    };

    this.enableHotkeyBinding = function() {
        var that = this;

        $(window).click(function() {
            that.addHotkeyInput();
        });

        $(document)
            .mouseout(function(e) {
                if (e.relatedTarget != null) return;
                that.removeHotkeyInput();
                that.returnFocus();
            })
            .mouseenter(function() {
                that.addHotkeyInput();
            })
            .on('keydown', 'input', function(e) {
                that.onKeyDown(e);
            })
            .on('keyup', 'input', function(e) {
                that.onKeyUp(e);
            });
    };

    this.addHotkeyInput = function() {
        this.removeHotkeyInput();
        $('body').append('<input type="text" />').children('input').focus();
    };

    this.removeHotkeyInput = function() {
        $('input').remove();
    };

    this.returnFocus = function() {
        if (this.returnFocusEvent == null) {
            this.returnFocusEvent = new CSEvent("com.adobe.PhotoshopLoseFocus", "APPLICATION");
            this.returnFocusEvent.extensionId = this.extensionId;
        }
        csInterface.dispatchEvent(this.returnFocusEvent);
    };

    this.initColors = function() {
        this.colors = [];
        for (var i = 0; i < this.settings.PLATES_RAW_MAX_QUANTITY; i++) {
            this.colors[i] = [];
            for (var j = 0; j < this.settings.PLATES_RAW_MAX_QUANTITY; j++) {
                this.colors[i][j] = this.foregroundColor;
            }
        }
    };

    this.refreshColors = function() {
        var currentTemp = -(this.settings.tempStepDistance * this.settings.plateOneSideQuantity);
        var currentLightness = (this.settings.lumaStepDistance * this.settings.plateOneSideQuantity);

        for (var i = 0; i < this.settings.plateRawQuantity; i++) {
            for (var j = 0; j < this.settings.plateRawQuantity; j++) {
                this.colors[i][j] = ColorCalc.getWarmerColorBrightnessFix(ColorCalc.addToAllChannels(this.foregroundColor, currentLightness), currentTemp);
                currentLightness -= this.settings.lumaStepDistance;
            }
            currentTemp += this.settings.tempStepDistance;
            currentLightness = (this.settings.lumaStepDistance * this.settings.plateOneSideQuantity);
        }
    };

    this.initSaturationColors = function() {
        this.saturationColors = [];

        for (var i = 0; i < this.settings.PLATES_RAW_MAX_QUANTITY; i++) {
            this.saturationColors[i] = this.foregroundColor;
        }
    };

    this.refreshSaturationColors = function() {
        var currentSaturationStep = this.settings.saturationMaxStep;
        var saturationDecreaseStep = this.settings.saturationMaxStep / this.settings.plateOneSideQuantity;

        for (var i = 0; i < this.settings.plateRawQuantity; i++) {
            this.saturationColors[i] = ColorCalc.saturationMove(this.foregroundColor, currentSaturationStep);
            currentSaturationStep -= saturationDecreaseStep;
        }
    };

    this.refresh = function() {
        this.refreshColors();
        this.refreshSaturationColors();
    };

    this.draw = function() {
        var colors = $('.colors');
        colors.empty();

        for (var i = 0; i < this.settings.plateRawQuantity; i++) {
            colors.append('<div class="row"></div>');
            for (var j = 0; j < this.settings.plateRawQuantity; j++) {
                colors.children('.row:last-child').append('<div class="cell"></div>').children('.cell:last-child').css('background-color', ColorCalc.toRGBString(this.colors[j][i]));
            }
        }

        var saturation = $('.saturation');
        saturation.empty();
        for (var k = 0; k < this.settings.plateRawQuantity; k++) {
            saturation.append('<div class="row"></div>');
            saturation.children('.row:last-child').append('<div class="cell"></div>').children('.cell:last-child').css('background-color', ColorCalc.toRGBString(this.saturationColors[k]));
        }

        this.resize();
    };

    this.resize = function() {
        var colors = $('.colors');
        var saturation = $('.saturation');

        var screenWidth = $(window).width() - 10;
        var screenHeight = $(window).height() - 6;

        var cellSize;
        if (screenWidth > screenHeight * (this.settings.plateRawQuantity + 1) / this.settings.plateRawQuantity) {
            cellSize = screenHeight / this.settings.plateRawQuantity;
        } else {
            cellSize = screenWidth / (this.settings.plateRawQuantity + 1);
        }

        var borderSize = cellSize * this.settings.plateRawQuantity;
        colors.css('width',  borderSize).css('height', borderSize);
        saturation.css('width', cellSize).css('height', borderSize);

        colors.find('.cell').css('width', cellSize);
        saturation.find('.cell').css('width', cellSize);

        var middle = Math.ceil(this.settings.plateRawQuantity / 2);
        var borderColor = ColorCalc.isDark(this.foregroundColor) ? this.settings.LIGHT_BORDER_COLOR : this.settings.DARK_BORDER_COLOR;
        $('.colors .row:nth-child(' + middle +') .cell:nth-child(' + middle +')')
            .css('width', cellSize - 2)
            .css('height', cellSize - 2)
            .css('border', 'solid 1px ' + borderColor);
        $('.saturation .row:nth-child(' + middle +') .cell')
            .css('width', cellSize - 2)
            .css('height', cellSize - 2)
            .css('border', 'solid 1px ' + borderColor);

        this.addHotkeyInput();
    };

    this.onKeyDown = function(e) {
        switch (e.which) {
            case KeyCodes.NUMBER_1:
                this.removePlates();
                break;
            case KeyCodes.NUMBER_2:
                this.addPlates();
                break;
            case KeyCodes.NUMBER_3:
                this.settings.lumaStepDistance -= 1;
                this.settings.tempStepDistance -= 1;
                break;
            case KeyCodes.NUMBER_4:
                this.settings.lumaStepDistance += 1;
                this.settings.tempStepDistance += 1;
                break;
            case KeyCodes.NUMBER_5:
                this.decreaseSaturation();
                break;
            case KeyCodes.NUMBER_6:
                this.increaseSaturation();
                break;
        }
        this.refresh();
        this.draw();
    };

    this.onKeyUp = function(e) {
        var save = e.which === KeyCodes.NUMBER_1 || e.which === KeyCodes.NUMBER_2 || e.which === KeyCodes.NUMBER_3 ||
                   e.which === KeyCodes.NUMBER_4 || e.which === KeyCodes.NUMBER_5 || e.which === KeyCodes.NUMBER_6;
        if (!save) return;
        this.settings.save();
    };

    this.addPlates = function() {
        if (this.settings.plateOneSideQuantity >= this.settings.PLATES_ONE_SIDE_MAX_QUANTITY) { return; }

        this.settings.plateOneSideQuantity += 1;
        this.settings.plateRawQuantity = this.settings.plateOneSideQuantity * 2 + 1;
    };

    this.removePlates = function() {
        if (this.settings.plateOneSideQuantity <= 1) { return; }

        this.settings.plateOneSideQuantity -= 1;
        this.settings.plateRawQuantity = this.settings.plateOneSideQuantity * 2 + 1;
    };

    this.increaseSaturation = function() {
        this.settings.saturationMaxStep += 0.1;
        this.settings.saturationMaxStep = Math.min(1, this.settings.saturationMaxStep);
    };

    this.decreaseSaturation = function() {
        this.settings.saturationMaxStep -= 0.1;
        this.settings.saturationMaxStep = Math.max(-1, this.settings.saturationMaxStep);
    };

    this.changePanelBackgroundColor = function(bgColor) {
        $('body').css('background-color', ColorCalc.toRGBString(ColorCalc.combineTo24(bgColor.red, bgColor.green, bgColor.blue)));
    };

    this.loadForegroundColor = function(callback) {
        // callback(ColorCalc.combineTo24(127, 199, 255));
        csInterface.evalScript('getColor()', function(rgbString) {
            var rgb = ColorCalc.stringToRGB(rgbString);
            callback(ColorCalc.combineTo24(rgb[0], rgb[1], rgb[2]));
        })
    };
}
