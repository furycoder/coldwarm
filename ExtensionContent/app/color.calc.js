function ColorCalc() {}

ColorCalc = {

    stringToRGB: function(rgbString) {
        var colorParts = rgbString.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        colorParts.shift();
        return [colorParts.shift(), colorParts.shift(), colorParts.shift()];
    },

    combineTo24: function(red, green, blue) {
        return red << 16 | green << 8 | blue;
    },

    getRed24: function(color) {
        return color >>> 16;
    },

    getGreen24: function(color) {
        return color >>> 8 & 0xFF;
    },

    getBlue24: function(color) {
        return color & 0xFF;
    },

    toRGBString: function(color) {
        return 'rgb(' + this.getRed24(color) + ', ' + this.getGreen24(color) + ', ' + this.getBlue24(color) + ')';
    },

    getLumaGray: function(color) {
        return (0.2126 * this.getRed24(color)) + (0.7152 * this.getGreen24(color)) + (0.0722 * this.getBlue24(color));
    },

    validateColorChannel: function(colorChannel) {
        return Math.min(255, Math.max(0, colorChannel));
    },

    getBrightness: function(color) {
        return (this.getRed24(color) * 299 + this.getGreen24(color) * 587 + this.getBlue24(color) * 114) / 1000;
    },

    isDark: function(color) {
        return this.getBrightness(color) < 128;
    },

    addToAllChannels: function(color, value) {
        return this.combineTo24(
            this.validateColorChannel(this.getRed24(color) + value),
            this.validateColorChannel(this.getGreen24(color) + value),
            this.validateColorChannel(this.getBlue24(color) + value)
        );
    },

    getWarmerColorBrightnessFix: function(color, distance) {
        var r = this.getRed24(color);
        var g = this.getGreen24(color);
        var b = this.getBlue24(color);

        var startBrightness = this.getLumaGray(color);

        r = this.validateColorChannel(r + distance);
        b = this.validateColorChannel(b - distance);
        var result = this.combineTo24(r, g, b);

        var endBrightness = this.getLumaGray(result);
        return this.addToAllChannels(result, (startBrightness - endBrightness) * 2);
    },

    saturationMove: function(color, value) {
        var rgb = [this.getRed24(color), this.getGreen24(color), this.getBlue24(color)];

        //Индексы массива
        var red = 0, green = 1, blue = 2;

        //Если серый -- нечего делать. Если насыщенность 100% -- нечего делать. Если изменение 0 -- нечего делать. Если отключили свет -- нечего делать.
        var isGray = rgb[red] === rgb[green] && rgb[red] === rgb[blue] && rgb[green] === rgb[blue];
        var isSaturationMax = (rgb[red] === 0 || rgb[blue] === 0 || rgb[green] === 0) && value > 0;

        if (isGray || isSaturationMax || value === 0) {
            return color;
        } else {
            value = Math.min(1, Math.max(-1, value));
        }

        var h = (rgb[red] > rgb[green]) ? red : green;
        h = (rgb[h] > rgb[blue]) ? h : blue;

        var l = (rgb[red] < rgb[green]) ? red : green;
        l = (rgb[l] > rgb[blue]) ? blue : l;

        var m = (rgb[red] > rgb[green] && rgb[red] < rgb[blue]) || (rgb[red] > rgb[blue] && rgb[red] < rgb[green])
            ? red
            : ((rgb[green] > rgb[red] && rgb[green] < rgb[blue]) || (rgb[green] > rgb[blue] && rgb[green] < rgb[red]))
                ? green
                : blue;

        var medium = this.getLumaGray(color);

        if (value > 0) {
            rgb[m] -= (rgb[h] - rgb[m]) / (rgb[h] - rgb[l]) * rgb[l] * value;
            rgb[l] -= rgb[l] * value;
        } else {
            rgb[l] += (rgb[l] - medium) * value;
            rgb[m] += (rgb[m] - medium) * value;
            rgb[h] += (rgb[h] - medium) * value;
        }

        return this.combineTo24(rgb[red], rgb[green], rgb[blue]);
    }
};