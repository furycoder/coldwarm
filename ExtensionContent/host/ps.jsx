function setColor(red, green, blue) {
    const color = new SolidColor();
    color.rgb.red = red;
    color.rgb.green = green;
    color.rgb.blue = blue;

    app.foregroundColor = color;
}

function getColor() {
    const color = app.foregroundColor;
    return 'rgb(' + Math.round(color.rgb.red) + ', ' + Math.round(color.rgb.green) + ', ' + Math.round(color.rgb.blue) + ')';
}

function makeCodeANumber(str) {
    return charIDToTypeID(str);
}